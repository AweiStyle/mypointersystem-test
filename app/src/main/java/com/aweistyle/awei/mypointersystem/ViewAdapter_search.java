package com.aweistyle.awei.mypointersystem;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ViewAdapter_search extends BaseAdapter {
    String items[][];

    public ViewAdapter_search(String items[][]){

        this.items = items;

    }


    @Override
    public int getCount() {

        return items.length;

    }

    @Override
    public Object getItem(int position) {

        return items[position];

    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.template2, null);
        }


//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        View view = inflater.inflate(R.layout.template2,null);

            TextView productCode = convertView.findViewById(R.id.productCode);
            TextView productName = convertView.findViewById(R.id.productName);
            TextView productSpecification = convertView.findViewById(R.id.productSpecification);
            TextView pointQuantity = convertView.findViewById(R.id.pointQuantity);
            TextView pointerVNO = convertView.findViewById(R.id.pointerVNO);

            productCode.setText(items[position][0]);
            productName.setText(items[position][1]);
            productSpecification.setText(items[position][2]);
            pointQuantity.setText(items[position][3] + items[position][5]);
            pointerVNO.setText(items[position][4]);


            return convertView;


    }
}
