package com.aweistyle.awei.mypointersystem;

public class RandomString {

    public static String randomString(int len) {
        String str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            int idx = (int)(Math.random() * str.length());
            sb.append(str.charAt(idx));
        }

        String sc = sb.toString();

        String ranStr = sc.substring(0,8) + "-" + sc.substring(8,12) + "-" + sc.substring(12,16) + "-" + sc.substring(16,20) + "-" + sc.substring(20,32);

        return ranStr;
    }

}
