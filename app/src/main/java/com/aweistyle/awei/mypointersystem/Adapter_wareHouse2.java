package com.aweistyle.awei.mypointersystem;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Adapter_wareHouse2 extends BaseAdapter {

    String[][] warehouseTag;

    public Adapter_wareHouse2(String warehouseTag[][]){

        this.warehouseTag = warehouseTag;

    }



    @Override
    public int getCount() {
        return warehouseTag.length;
    }

    @Override
    public Object getItem(int position) {
        return warehouseTag[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.spinner_item2,null);

        TextView company = view.findViewById(R.id.txtSpn_getView1);
        TextView wareHouse = view.findViewById(R.id.txtSpn_getView2);

        company.setText(warehouseTag[position][0]);
        wareHouse.setText(warehouseTag[position][1]);

        return view;


    }
}
