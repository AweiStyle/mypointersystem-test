package com.aweistyle.awei.mypointersystem;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ViewAdapter_stockSearch extends BaseAdapter {

    String items[][];

    public ViewAdapter_stockSearch(String items[][]){

        this.items = items;

    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.template3, null);
        }

//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        View view = inflater.inflate(R.layout.template3,null);

        TextView productCode = convertView.findViewById(R.id.productCode);
        TextView productName = convertView.findViewById(R.id.productName);
        TextView productSpecification = convertView.findViewById(R.id.productSpecification);
//        TextView productUnit = view.findViewById(R.id.productUnit);
        TextView stockQTY = convertView.findViewById(R.id.stockQTY);
        TextView stockName = convertView.findViewById(R.id.stockName);
        TextView totalQTY = convertView.findViewById(R.id.totalQTY);

        productCode.setText(items[position][0]);
        productName.setText(items[position][1]);
        productSpecification.setText(items[position][2]);
//        productUnit.setText(items[position][3]);
        stockQTY.setText(items[position][4] + items[position][3]);
        stockName.setText(items[position][5] + "倉庫存:");
        totalQTY.setText(items[position][6] + items[position][3]);

        return convertView;
    }
}
