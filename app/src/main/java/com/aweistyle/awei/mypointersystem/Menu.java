package com.aweistyle.awei.mypointersystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Menu extends AppCompatActivity {

    String companyName;
    String version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        final ImageButton point = findViewById(R.id.btn_point);
        final ImageButton search = findViewById(R.id.btn_search);
        final ImageButton stockSearch = findViewById(R.id.btn_stockSearch);

        companyName = getSharedPreferences("company_version", MODE_PRIVATE).getString("companyName", "");
        version = getSharedPreferences("company_version", MODE_PRIVATE).getString("version", "");

        TextView versionName = findViewById(R.id.version);
        versionName.setText( " " + companyName + version);

        point.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){

//                    ((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.inventory_icon));
                    point.setBackgroundResource(R.drawable.inventory_test2);


                }else if(event.getAction() == MotionEvent.ACTION_UP){

//                    ((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.inventory_icon));
                    point.setBackgroundResource(R.drawable.inventory_test1);
                    Intent intent = new Intent(Menu.this,StartPointer.class);
                    startActivity(intent);

                }

                return false;
            }
        });

        search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){

//                    ((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.inventory_icon));
                    search.setBackgroundResource(R.drawable.search2);


                }else if(event.getAction() == MotionEvent.ACTION_UP){

//                    ((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.inventory_icon));
                    search.setBackgroundResource(R.drawable.search1);
                    Intent intent = new Intent(Menu.this,StartSearch.class);
                    startActivity(intent);

                }

                return false;
            }
        });

        stockSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){

//                    ((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.inventory_icon));
                    stockSearch.setBackgroundResource(R.drawable.stocksearch2);


                }else if(event.getAction() == MotionEvent.ACTION_UP){

//                    ((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.inventory_icon));
                    stockSearch.setBackgroundResource(R.drawable.stocksearch1);
                    Intent intent = new Intent(Menu.this,StockSearch.class);
                    startActivity(intent);

                }

                return false;
            }
        });

    }


}
