package com.aweistyle.awei.mypointersystem;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LoginPage extends AppCompatActivity {

    EditText DBAccount_ET;
    EditText DBPassword_ET;
    EditText connectString_ET;
    EditText DBName_ET;

    String companyName;

    String url;
    String DBName;
    String user;
    String password;
    String a;
    String version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        Button btn_connection = findViewById(R.id.connection);
        Button btn_textConnection = findViewById(R.id.textConnection);
        Button btn_finish = findViewById(R.id.finish);
//        Button developer1 = findViewById(R.id.developer1);
        DBName_ET = findViewById(R.id.R_id_ET_DBName);
        DBAccount_ET = findViewById(R.id.ET_DBAccount);
        DBPassword_ET = findViewById(R.id.ET_DBPassword);
        connectString_ET = findViewById(R.id.ET_connectString);

        version = getVersionName(this);

//        if (haveInternet() == false){
//
//            Log.e("testConnection","noInternet");
//
//            Toast toast = Toast.makeText(LoginPage.this, "請確認網路是否連接", Toast.LENGTH_LONG);
//            toast.show();
//
//        }

        SharedPreferences sharedPreferences = getSharedPreferences("Log", MODE_PRIVATE);
        a = sharedPreferences.getString("LoginSuccess", "firstLogin");

        Log.e("read tempSave =", a);

        if (a.equals("LoginSuccess") ) {


            Intent intent = new Intent(LoginPage.this, PointerPage.class);
            startActivity(intent);

            finish();

        }



        btn_connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new Thread(runnable_loginDB).start();


            }
        });

        btn_textConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new Thread(runnable_testLogin).start();


            }
        });

        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

//        developer1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent();
//                intent.setClass(LoginPage.this, PointerPage.class);
//                startActivity(intent);
//
//            }
//        });

    }

    @Override
    public void onBackPressed() {

    }

    Runnable runnable_loginDB = new Runnable() {
        @Override
        public void run() {

            try {

                if (haveInternet() == false){

                    Log.e("testConnection","noInternet");

                    throw new ConnectException();

                }

                user = DBAccount_ET.getText().toString();
                password = DBPassword_ET.getText().toString();
                url = connectString_ET.getText().toString();
                DBName = DBName_ET.getText().toString();

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");

                new Thread(company).start();


                    SharedPreferences sharedPreferences = getSharedPreferences("Log", MODE_PRIVATE);
                    sharedPreferences.edit().putString("LoginSuccess", "LoginSuccess")
                            .putString("DBName",DBName)
                            .putString("user",user)
                            .putString("password",password)
                            .putString("url",url).commit();

                    Log.e("commit:","LoginSuccess");


                    finish();

                    Intent intent = new Intent(LoginPage.this, PointerPage.class);
                    startActivity(intent);


            } catch (ClassNotFoundException e) {

                Log.e("catch:","catch1");




                e.printStackTrace();
            } catch (SQLException e) {
                Log.e("catch:","catch2"+ e.getMessage()+e.getErrorCode());

                if(e.getErrorCode() == 18456){

                    Looper.prepare();
                    Toast toast = Toast.makeText(LoginPage.this, "請確認帳號密碼是否正確", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                }else if (e.getErrorCode() == 0){

                    Looper.prepare();
                    Toast toast = Toast.makeText(LoginPage.this, "請確認連線字串是否正確", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                }else if(e.getErrorCode() == 4060){

                    Looper.prepare();
                    Toast toast = Toast.makeText(LoginPage.this, "請確認資料庫名稱是否正確", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                }


                e.printStackTrace();
            } catch (ConnectException e) {

                Looper.prepare();
                Toast toast = Toast.makeText(LoginPage.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                toast.show();
                Looper.loop();
                e.printStackTrace();

            }


        }

    };

    Runnable runnable_testLogin = new Runnable() {
        @Override
        public void run() {

            try {

                try {

                    if (haveInternet() == false){

                        Log.e("testConnection","noInternet");

                        throw new ConnectException();

                    }

                user = DBAccount_ET.getText().toString();
                password = DBPassword_ET.getText().toString();
                url = connectString_ET.getText().toString();
                DBName = DBName_ET.getText().toString();

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");

                Statement statement = con.createStatement();
                ResultSet login = statement.executeQuery("SELECT STORNA,STORNAME,STORNO FROM dbo.STORNA;");

                if (login.next()) {

                    Looper.prepare();
                    Toast toast = Toast.makeText(LoginPage.this, "連線成功", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();


                }



                login.close();
                statement.close();

            } catch (ClassNotFoundException e) {






                e.printStackTrace();

            } catch (SQLException e) {

                    Looper.prepare();
                    Toast toast = Toast.makeText(LoginPage.this, "連線失敗", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                e.printStackTrace();
            }


        }catch (ConnectException e){

                Looper.prepare();
                Toast toast = Toast.makeText(LoginPage.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                toast.show();
                Looper.loop();

            }
        }

    };

    private boolean haveInternet()
    {
        boolean result = false;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=connManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
        {
            result = false;
        }
        else
        {
            if (!info.isAvailable())
            {
                result =false;
            }
            else
            {
                result = true;
            }
        }

        return result;
    }

    Runnable company = new Runnable() {
        @Override
        public void run() {

            try {

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");

                Statement statement = con.createStatement();
                ResultSet company = statement.executeQuery("SELECT top 1 company from CompanyInfo");

                while (company.next()) {

                    companyName = company.getString(1);


                }

                SharedPreferences sharedPreferences = getSharedPreferences("company_version", MODE_PRIVATE);
                sharedPreferences.edit().putString("companyName",companyName)
                        .putString("version",version).commit();

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    };

    public String getVersionName(Context context){

        PackageManager packageManager=context.getPackageManager();
        PackageInfo packageInfo;
        String versionName="";
        try {
            packageInfo=packageManager.getPackageInfo(context.getPackageName(),0);
            versionName=packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

}
