package com.aweistyle.awei.mypointersystem;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

public class StartSearch extends AppCompatActivity {

    String[][] items ;
    Spinner spinner1;
    String url;
    String DBName;
    String user;
    String password;
    Adapter_wareHouse adapter_wareHouse;
    ListView searchList;
    String barcode;
    String productName1;
    String spec;
    String stockQTY;
    String pointerQTY;
    String finalPointer;
    String pointerVNO;
    ArrayList<String> arrayList =new ArrayList<>();
    ViewAdapter_search viewAdapter_search;
    String scanResult;
    Runnable getData_scan;
    String storeID_choose;
    int storeID_chooseInt;
    String pointBefore;
    String unit;
    String[][] empty = {};

    ArrayList<String> storeID = new ArrayList<>();
    ArrayList<String> autoComplete = new ArrayList<>();
    AutoCompleteTextView autoCompleteTextView1;
    Thread thread;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "退出掃描", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, result.getBarcodeImagePath(), Toast.LENGTH_SHORT).show();
                scanResult = result.getContents();
                checkData(scanResult);

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_search);

        if (haveInternet() == false){

            Log.e("testConnection","noInternet");

            Toast toast = Toast.makeText(StartSearch.this, "請確認網路是否連接", Toast.LENGTH_LONG);
            toast.show();

        }

        Button bt_1 = findViewById(R.id.BT1);
        final Activity activity = this;
        spinner1 = findViewById(R.id.spinner1);
        autoCompleteTextView1 = findViewById(R.id.autoCompleteTextView1);

        DBName = getSharedPreferences("Log", MODE_PRIVATE).getString("DBName", "");
        user = getSharedPreferences("Log", MODE_PRIVATE).getString("user", "");
        password = getSharedPreferences("Log", MODE_PRIVATE).getString("password", "");
        url = getSharedPreferences("Log", MODE_PRIVATE).getString("url", "");

        Log.e("tag", "EditPage頁面成功取得DBName = " + DBName);
        Log.e("tag", "EditPage頁面成功取得user = " + user);
        Log.e("tag", "EditPage頁面成功取得password = " + password);
        Log.e("tag", "EditPage頁面成功取得url = " + url);

        storeID_choose = getSharedPreferences("store_login", MODE_PRIVATE).getString("storeID", "");
        String storeID_chooseTemp = getSharedPreferences("store_login", MODE_PRIVATE).getString("storeID_int", "");
        storeID_chooseInt = Integer.parseInt(storeID_chooseTemp);
        Log.e("storeID_chooseInt =" , storeID_chooseTemp);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                new Thread(getData).start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        autoCompleteTextView1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (thread != null){

                    thread.interrupt();

                }

                thread = new Thread(getData);
                thread.start();

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (haveInternet() == false){

            Log.e("testConnection","noInternet");

            Toast toast = Toast.makeText(StartSearch.this, "請確認網路是否連接", Toast.LENGTH_LONG);
            toast.show();

        }

            }
        });




        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                try {



                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                            + "databaseName=" + DBName + ";user=" + user
                            + ";password=" + password + ";");

                    Statement statement = con.createStatement();
                    ResultSet store = statement.executeQuery("SELECT STORNA,STORNAME,STORNO FROM dbo.STORNA;");

                    ArrayList<String[]> arrayList = new ArrayList<>();
                    while (store.next()) {

                        String storeA = store.getString(1);
                        String storeB = store.getString(2);
                        //Log.e("Allen", storeA + "" + storeB);

                        arrayList.add(new String[]{storeA, storeB});

                        storeID.add(store.getString(3));
                    }

                    spinner1 = findViewById(R.id.spinner1);

                    String[][] warehouse = arrayList.toArray(new String[][]{});
                    adapter_wareHouse = new Adapter_wareHouse(warehouse);

                    Runnable r = new Runnable() {
                        @Override
                        public void run() {

                            spinner1.setAdapter(adapter_wareHouse);
                            spinner1.setSelection(storeID_chooseInt);

                        }
                    };

//                    String[] storeNames = {"A", "B", "C"};
//                    String storeName =  storeNames[spinner1.getSelectedItemPosition()];
//
//                    "select XXX from XXX where STORENO = '" + storeName + "'";


                    runOnUiThread(r);

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }
        };

        new Thread(runnable).start();

        bt_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Allen", "scan1");

                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();

            }
        });

    }

    Runnable r4 = new Runnable() {
        @Override
        public void run() {

            viewAdapter_search = new ViewAdapter_search(empty);
            searchList.setAdapter(viewAdapter_search);

        }
    };

    public void checkData(final String code) {

        getData_scan = new Runnable() {
            @Override
            public void run() {

                try {

                    if (haveInternet() == false){

                        Log.e("testConnection","noInternet");

                        throw new ConnectException();

                    }

                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"
                            + "databaseName=" + DBName + ";user=" + user
                            + ";password=" + password + ";");

                    String query_scan = "select top 100 PART.PARTNO , PART.PARTNA , PART.SPEC , STKADJS.STKQTY , STKADJS.NEWQTY , STKADJH.EDUSER , STKADJS.VNO , PART.UNIT from STKADJH join STKADJS ON STKADJH.SN = STKADJS.HEADSN join PART ON PART.partno = STKADJS.partno WHERE PART.BarcodeNO = '" + code + "' AND STKADJH.STORNO = '" + storeID_choose + "' AND PART.ISDel = 'FALSE' order by STKADJH.VDATE desc"  ;


                    Log.e("Start_search_checkData:", query_scan);


                    Statement statement = con.createStatement();
                    ResultSet resultSet = statement.executeQuery(query_scan);

                    DecimalFormat df = new DecimalFormat("##0.0");

                    final ArrayList<String[]> arrayList = new ArrayList<>();
                    while (resultSet.next()) {

                        Log.e("checkData:","if");

                        barcode = resultSet.getString(1);
                        productName1 = resultSet.getString(2);
                        spec = resultSet.getString(3);
//                        stockQTY = resultSet.getString(4);
                        pointerQTY = resultSet.getString(5);
//                        finalPointer = resultSet.getString(6);
                        pointerVNO = resultSet.getString(7);
                        unit = resultSet.getString(8);

                        Double.valueOf(pointerQTY);
                        String pointerQTY1 = df.format(Double.valueOf(pointerQTY));

                        arrayList.add(new String[]{barcode, productName1 , spec  , pointerQTY1  , pointerVNO , unit});

                    }

                    Runnable r3 = new Runnable() {
                        @Override
                        public void run() {

                            TextView show = findViewById(R.id.show);
                            show.setText("共搜尋到"+arrayList.size()+"筆資料");

                        }
                    };
                    runOnUiThread(r3);


                    if (arrayList.size() == 0) throw new SQLException();

                    searchList = findViewById(R.id.searchList);

                    items = arrayList.toArray(new String[][]{});
                    viewAdapter_search = new ViewAdapter_search(items);

                } catch (ClassNotFoundException e) {

                    Log.e("checkData:","catch1");

                    e.printStackTrace();
                } catch (SQLException e) {

                    Log.e("checkData:","catch2");

                    runOnUiThread(r4);

                    Looper.prepare();
                    Toast toast = Toast.makeText(StartSearch.this, "尚未盤點過此商品", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                    e.printStackTrace();
                }catch (ConnectException e){

                    Looper.prepare();
                    Toast toast = Toast.makeText(StartSearch.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                }


                Runnable r2 = new Runnable() {
                    @Override
                    public void run() {

                        Log.e("tag","setView");

                        searchList.setAdapter(viewAdapter_search);

                    }
                };
                runOnUiThread(r2);



            }
        };

        new Thread(getData_scan).start();

    }

    Runnable getData = new Runnable() {
        @Override
        public void run() {

            try {

                Thread.sleep(500);

                if (haveInternet() == false){

                    Log.e("testConnection","noInternet");

                    throw new ConnectException();

                }

                if(storeID.size() == 0){

                    Looper.prepare();
                    Toast toast = Toast.makeText(StartSearch.this, "如已接上網路.請關閉頁面後重試", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                    return;

                }

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"
                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");

                String query = "select top 100 STKADJS.PARTNO,PART.PARTNA,PART.SPEC,STKADJS.NEWQTY , STKADJH.StaffNA , STKADJS.STKQTY , STKADJS.VNO , PART.unit from STKADJS join STKADJH ON STKADJH.SN = STKADJS.HEADSN join PART ON PART.partno = STKADJS.partno WHERE STKADJH.STORNO = '" + storeID.get(spinner1.getSelectedItemPosition()) + "' AND PART.PARTNA LIKE '%" + autoCompleteTextView1.getText() + "%' order by STKADJH.VDATE desc";


                Log.e("getData_Query:", query);


                Statement statement = con.createStatement();
                ResultSet resultSet = statement.executeQuery(query);

                autoComplete.clear();
                DecimalFormat df = new DecimalFormat("##0.0");
                final ArrayList<String[]> arrayList = new ArrayList<>();

                while (resultSet.next()) {

                    barcode = resultSet.getString(1);
                    productName1 = resultSet.getString(2);
                    spec = resultSet.getString(3);
//                    stockQTY = resultSet.getString(6);
//                    finalPointer = resultSet.getString(5);
                    pointerQTY =resultSet.getString(4);
                    pointerVNO = resultSet.getString(7);
                    unit = resultSet.getString(8);

                    String name = resultSet.getString(2);

                    Double.valueOf(pointerQTY);
                    String pointerQTY1 = df.format(Double.valueOf(pointerQTY));

                    arrayList.add(new String[]{barcode , productName1 , spec  , pointerQTY1  , pointerVNO , unit });

                    if (!autoComplete.contains(name)) autoComplete.add(name);

                }

                Runnable r3 = new Runnable() {
                    @Override
                    public void run() {

                        TextView show = findViewById(R.id.show);
                        show.setText("共搜尋到"+arrayList.size()+"筆資料");

                    }
                };
                runOnUiThread(r3);




                searchList = findViewById(R.id.searchList);

                items = arrayList.toArray(new String[][]{});
                viewAdapter_search = new ViewAdapter_search(items);

                Gson gson = new Gson();
                String a = gson.toJson(items);

                Log.e("tag",a);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Log.e("tag","autoComplete");

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                                StartSearch.this,
                                android.R.layout.simple_dropdown_item_1line,
                                autoComplete); //宣告 Adapter
                        autoCompleteTextView1.setAdapter(adapter); //設定 Adapter 給 mAutoCompleteTextView
                        autoCompleteTextView1.setThreshold(1); //設定輸入幾個字後開始比對
//        autoCompleteTextView.setDropDownBackgroundResource(R.drawable.yellow);//設定背景圖片
//        autoCompleteTextView.setDropDownBackgroundDrawable(getResource().getDrawable(R.drawable.yellow));//設定背景圖片
                        autoCompleteTextView1.setDropDownWidth(400); //設定寬度
                        autoCompleteTextView1.setDropDownHeight(400); //設定高度
//        autoCompleteTextView.setOnItemClickListener(AutoCompleteTextViewOnItemClickListener);
                    }
                });



            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }catch (ConnectException e){

                Log.e("getData catch:","ConnectException");

                Looper.prepare();
                Toast toast = Toast.makeText(StartSearch.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                toast.show();
                Looper.loop();

            } catch (InterruptedException e) {

                return;

            }


            Runnable r1 = new Runnable() {
                @Override
                public void run() {

                    Log.e("tag","setView");

                    searchList.setAdapter(viewAdapter_search);

                }
            };

            runOnUiThread(r1);

        }
    };

    private boolean haveInternet()
    {
        boolean result = false;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=connManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
        {
            result = false;
        }
        else
        {
            if (!info.isAvailable())
            {
                result =false;
            }
            else
            {
                result = true;
            }
        }

        return result;
    }

}
