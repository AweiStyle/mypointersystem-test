package com.aweistyle.awei.mypointersystem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PointerPage extends AppCompatActivity {
    Spinner spinner_storeNumber;
    Adapter_wareHouse2 adapter_wareHouse2;
    ArrayList<String> storeID = new ArrayList<>();
    String storeID_choose;

    String storeA;
    String storeB;
    String companyName;
    String version;

    String url;
    String DBName;
    String user;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pointer_page);

        Button enter = findViewById(R.id.btn_enter);
        Button exit = findViewById(R.id.btn_exit);

        if (haveInternet() == false){

            Log.e("testConnection","noInternet");

            Toast toast = Toast.makeText(PointerPage.this, "請確認網路是否連接", Toast.LENGTH_LONG);
            toast.show();

        }


        DBName = getSharedPreferences("Log", MODE_PRIVATE).getString("DBName", "");
        user = getSharedPreferences("Log", MODE_PRIVATE).getString("user", "");
        password = getSharedPreferences("Log", MODE_PRIVATE).getString("password", "");
        url = getSharedPreferences("Log", MODE_PRIVATE).getString("url", "");

        companyName = getSharedPreferences("company_version", MODE_PRIVATE).getString("companyName", "");
        version = getSharedPreferences("company_version", MODE_PRIVATE).getString("version", "");

        Log.e("tag", "PointerPage頁面成功取得DBName = " + DBName);
        Log.e("tag", "PointerPage頁面成功取得user = " + user);
        Log.e("tag", "PointerPage頁面成功取得password = " + password);
        Log.e("tag", "PointerPage頁面成功取得url = " + url);

        TextView versionName = findViewById(R.id.version);
        versionName.setText( " " + companyName + version);

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText account = findViewById(R.id.account);
                final EditText passWord = findViewById(R.id.passWord);

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        try {

                            Class.forName("net.sourceforge.jtds.jdbc.Driver");
                            Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                                    + "databaseName=" + DBName + ";user=" + user
                                    + ";password=" + password + ";");

                            Statement statement = con.createStatement();

                            String query = "SELECT * FROM dbo.ZSTAFF WHERE Account = '" + account.getText() + "' AND Password = '" + passWord.getText() + "';";
                            Log.e("ALLen", query);

                            ResultSet resultAccPass = statement.executeQuery(query);


                            if (resultAccPass.next()) {


                                Intent intent = new Intent(PointerPage.this, Menu.class);
                                startActivity(intent);


                                String account = resultAccPass.getString(6);
                                String password = resultAccPass.getString(7);
                                String staffName = resultAccPass.getString(2);
                                storeID_choose = storeID.get(spinner_storeNumber.getSelectedItemPosition());
                                int storeID_chooseInt = spinner_storeNumber.getSelectedItemPosition();


                                Log.e("account", account);
                                Log.e("password", password);
                                Log.e("staff", staffName);
                                Log.e("storeID_choose", storeID_choose);

                                SharedPreferences sharedPreferences = getSharedPreferences("store_login", MODE_PRIVATE);
                                sharedPreferences.edit().putString("staffID",account)
                                        .putString("staffName",staffName)
                                        .putString("storeID",storeID_choose)
                                        .putString("storeID_int",String.valueOf(storeID_chooseInt)).commit();



                            } else {

                                new Thread() {

                                    public void run()

                                    {

                                        Looper.prepare();
                                        Toast toast = Toast.makeText(PointerPage.this, "帳號或密碼錯誤", Toast.LENGTH_LONG);
                                        toast.show();
                                        Looper.loop();

                                    }

                                }.start();

                            }


                            resultAccPass.close();
                            statement.close();

                        } catch (SQLException e) {

                            Looper.prepare();
                            Toast toast = Toast.makeText(PointerPage.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                            toast.show();
                            Looper.loop();

                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                        try {

                            Class.forName("net.sourceforge.jtds.jdbc.Driver");
                            Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                                    + "databaseName=" + DBName + ";user=" + user
                                    + ";password=" + password + ";");

                            Statement statement = con.createStatement();

                            String query = "SELECT * FROM dbo.STORNA WHERE STORNO = '" + storeID_choose + "' ;";

                            ResultSet resultStoreName = statement.executeQuery(query);


                            while (resultStoreName.next()) {

                                String storeName = resultStoreName.getString(4);



                                SharedPreferences sharedPreferences = getSharedPreferences("store_login", MODE_PRIVATE);
                                sharedPreferences.edit().putString("storeName",storeName).commit();

                            }


                            resultStoreName.close();
                            statement.close();

                        } catch (SQLException e) {

                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                };

                new Thread(runnable).start();


            }

        });


        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

//        developer2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(PointerPage.this, Menu.class);
//                startActivity(intent);
//            }
//        });



        Runnable runnable_spinner = new Runnable() {
            @Override
            public void run() {

                try {

                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                            + "databaseName=" + DBName + ";user=" + user
                            + ";password=" + password + ";");

                    Statement statement = con.createStatement();
                    ResultSet store = statement.executeQuery("SELECT STORNA,STORNAME,STORNO FROM dbo.STORNA;");

                    ArrayList<String[]> arrayList = new ArrayList<>();
                    while (store.next()) {

                        storeA = store.getString(1);
                        storeB = store.getString(2);
                        //Log.e("Allen", storeA + "" + storeB);

                        arrayList.add(new String[]{storeA, storeB});
                        storeID.add(store.getString(3));
                    }

                    spinner_storeNumber = findViewById(R.id.spinner_storeNumber);

                    String[][] warehouse = arrayList.toArray(new String[][]{});
                    adapter_wareHouse2 = new Adapter_wareHouse2(warehouse);

                    Runnable r = new Runnable() {
                        @Override
                        public void run() {

                            spinner_storeNumber.setAdapter(adapter_wareHouse2
                            );
                            spinner_storeNumber.setSelection(0);
                        }
                    };

                    runOnUiThread(r);




                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }
        };

        new Thread(runnable_spinner).start();


    }

    @Override
    public void onBackPressed() {

    }

    private boolean haveInternet()
    {
        boolean result = false;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=connManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
        {
            result = false;
        }
        else
        {
            if (!info.isAvailable())
            {
                result =false;
            }
            else
            {
                result = true;
            }
        }

        return result;
    }


}


